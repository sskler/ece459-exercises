use std::thread;

static N: u32 = 1000;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children : Vec<thread::JoinHandle<_>> = vec![];

    for thread_num in 0..N {
        children.push(thread::spawn(move || {
            println!("Thread {} is running", thread_num)
        }))
    }

    for child in children {
        child.join();
    }
}

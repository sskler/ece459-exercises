// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 {
    let mut sum:i32 = 0;
    for value in 0..number {
       if  (value % multiple1 == 0) || (value % multiple2 == 0) {
           sum += value;
       }
    }

    return sum
}


fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}

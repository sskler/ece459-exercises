// You should implement the following function

fn fibonacci_number(n: u32) -> u32 {
    return ((((1.61815754 as f64).powi(n as i32)) - ((-0.61815754 as f64).powi(n as i32)))/(5 as f64).sqrt()) as u32
}


fn main() {
    println!("{}", fibonacci_number(8));
}

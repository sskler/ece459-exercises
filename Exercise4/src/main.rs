use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx):(Sender<i32>, Receiver<i32>) = mpsc::channel();
    let mut children : Vec<thread::JoinHandle<_>> = vec![];

    for thread_num in 0..10 {
        let tx_copy:Sender<i32> = tx.clone();
        children.push(thread::spawn(move || {
            tx_copy.send(thread_num)
        }));

        thread::sleep(Duration::from_secs(1))
    }

    for child in children {
        child.join();
    }

    for x in 0..10{
        println!("Got: {}", rx.recv().unwrap());
    }
}
